from django.http import HttpRequest
from django.test import TestCase
from unittest.mock import patch
from monitoring.managedb.update import update_racks
from monitoring.views import datacentre_view

# View tests are probably not as useful as any other unit tests
# Since the populated view is tested by the functional tests
class MonitoringViewTestCase(TestCase):

    # Objects to mock out - these are passed into the test functions
    @patch('monitoring.managedb.update.update_racks')

    def test_GIVEN_request_WHEN_datacentre_view_THEN_page_shows_datacentre(self, mock_update_racks):

        mock_update_racks.return_value = None
        
        request = HttpRequest()
        response = datacentre_view(request)

        mock_update_racks.assert_called_once()
        self.assertIn(b'<h1>Datacentre View</h1>', response.content)

