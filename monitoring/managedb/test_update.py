from django.test import TestCase
from unittest.mock import patch
from monitoring.managedb.update import update_racks
from monitoring.models import Rack

# Tests the application logic coded in the update file
class ManageDBUpdateTestCase(TestCase):

    # Objects to mock out - these are passed into the test functions
    @patch('monitoring.managedb.update.get_response_from_json')
    @patch('monitoring.models.Rack')

    def test_GIVEN_racks_WHEN_no_racks_THEN_racks_created(self, mock_racks_response, mock_rack_table):

        # Configure mock response to return 2 racks, but DB to have none
        mock_racks_response.return_value = """{ 'racks': [
            {'component_id': 1, 'component_type': 'rack',},
            {'component_id': 2, 'component_type': 'rack',},
            ] }"""
        mock_rack_table.objects = None

        # Call to get racks
        update_racks()

        # Check
        mock_racks_response.assert_called_once()

