from django import forms


class EmailForm(forms.Form):

    subject = forms.CharField(max_length=100,
                              required=True)
    message = forms.CharField(widget=forms.Textarea)
    recipient = forms.EmailField(required=True)
    sender = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        self.email = kwargs.pop('email', None)
        super(EmailForm, self).__init__(*args, **kwargs)
        self.fields['sender'].initial = self.email.sender


class EmailSettingsForm(forms.Form):

    host = forms.CharField(max_length=100)
    port = forms.IntegerField()
    user = forms.CharField(max_length=30, required=False)
    password = forms.CharField(widget=forms.PasswordInput(), max_length=50, required=False)
    use_tls = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        self.email = kwargs.pop('email', None)
        super(EmailSettingsForm, self).__init__(*args, **kwargs)
        self.fields['host'].initial = self.email.host
        self.fields['port'].initial = self.email.port
        self.fields['user'].initial = self.email.user
        self.fields['password'].initial = self.email.password
        self.fields['use_tls'].initial = self.email.use_tls
