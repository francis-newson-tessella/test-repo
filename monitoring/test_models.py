from copy import deepcopy
from django.test import TestCase
from monitoring.models import EmailSettings

# Model tests are less important if the model is kept clean
# from application logic
class MonitoringModelsTestCase(TestCase):

    def test_GIVEN_email_settings_WHEN_values_different_THEN_settings_saved(self):

        settings = EmailSettings()
        new_settings = deepcopy(settings)
        new_settings.host = 'some.host.there'
        new_settings.sender = 'some.one@host.there'

        new_settings.save()

        saved_items = EmailSettings.objects.all()
        self.assertEqual(saved_items.count(), 1)

        saved_item = saved_items[0]
        self.assertEqual(saved_item, new_settings)
        self.assertNotEqual(settings, new_settings)

