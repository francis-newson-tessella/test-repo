from django.apps import AppConfig

import json
import requests


class MonitoringConfig(AppConfig):
    name = 'monitoring'
