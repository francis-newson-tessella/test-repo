from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.conf import settings


class EmailSettings(models.Model):
    """
    Email settings - need to be stored in db so they can be changed manually.
    Default to the settings.py values
    """

    host = models.CharField(max_length=100, default=settings.EMAIL_HOST)
    port = models.IntegerField(default=settings.EMAIL_PORT)
    sender = models.EmailField(default=settings.EMAIL_SENDER)
    user = models.CharField(max_length=30,
                            default=settings.EMAIL_HOST_USER,
                            null=True,
                            blank=True)
    password = models.CharField(max_length=50,
                                default=settings.EMAIL_HOST_PASSWORD,
                                null=True,
                                blank=True)
    use_tls = models.BooleanField(default=settings.EMAIL_USE_TLS)


class Property(models.Model):

    property_name = models.CharField(max_length=30)
    property_value = models.CharField(max_length=30)
    property_type = models.CharField(max_length=30)

    monitored = models.BooleanField(default=False)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return '{0}: {1}'.format(self.property_name, self.property_value)


class ComponentCore(models.Model):

    component_type = models.CharField(max_length=30)
    component_id = models.IntegerField()

    FAULT = 'fault'
    ALARM = 'alarm'
    OK = 'ok'
    STATUS_CHOICES = (
        (FAULT, FAULT),
        (ALARM, ALARM),
        (OK, OK),
    )
    status = models.CharField(max_length=10,
                              choices=STATUS_CHOICES,
                              default=OK)
    properties = GenericRelation(Property, related_query_name='components')

    class Meta:
        abstract = True


class Rack(ComponentCore):

    def __str__(self):
        return 'Rack, ID={0}'.format(self.component_id)


class Component(ComponentCore):

    row = models.IntegerField()
    col = models.IntegerField()
    width = models.IntegerField()
    height = models.IntegerField()

    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return 'Type={0}, ID={1}'.format(self.component_type, self.component_id)


class Event(models.Model):

    event_id = models.CharField(max_length=50)
    name = models.CharField(max_length=30)
    type = models.CharField(max_length=30)
    severity = models.CharField(max_length=30)
    timestamp = models.DateTimeField()

    # events are only linked to component types
    component = models.ForeignKey(Component, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return '{0} ({1}), {2}: {3}'.format(self.name,
                                            self.type,
                                            self.severity,
                                            self.timestamp)


class Parameter(models.Model):

    name = models.CharField(max_length=30)
    type = models.CharField(max_length=30)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return '{0}: {1}'.format(self.type, self.name)


class Command(models.Model):

    name = models.CharField(max_length=30)
    parameters = GenericRelation(Parameter, related_query_name='commands')

    component = models.ForeignKey(Component, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return '{0}:\n{1}'.format(self.name, self.parameters)
