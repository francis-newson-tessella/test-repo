from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.datacentre_view, name='datacentre'),
    url(r'^rack/(?P<rack_number>[0-9]+)/$', views.rack_view, name='rack'),
    url(r'^rack/(?P<rack_number>[0-9]+)/(?P<component_type>\w+)/$', views.rack_view, name='rack'),
    url(r'^rack/(?P<rack_number>[0-9]+)/(?P<component_type>\w+)/(?P<component_id>[0-9]+)/$', views.rack_view, name='rack'),
    url(r'^email_test.html$', views.send_email_test, name='send_email_test'),
    url(r'^email_settings.html$', views.email_settings, name='email_settings'),
]

