#!/bin/bash
#default folders for deployment files
plugindir=$HOME/skadeploy/uwsgi-plugins
nginxdir=$HOME/skadeploy/nginx
uwsgidir=$HOME/skadeploy/uwsgi
socketdir=$HOME/skadeploy/nginx/sockets
here=$(pwd)

#default mountpoint and api version
mountpoint=/tm
apiversion=mock

#the message displayed when ./deploy -h is called
function show_help {
echo "Usage: deploy [-m MOUNTPOINT] [-a APIVERSION] [-s SOCKETDIR]"
echo "Mountpoint defaults to /tm"
echo "Socket is SOCKETDIR/MOUNTPOINT"
}

#Use commandline arguments to update default locations
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

while getopts "h?m:a:s:" opt; do
    case "$opt" in
        h|\?)
            show_help
            exit 0
            ;;
        m)  mountpoint=$OPTARG
            ;;
        a)  apiversion=$OPTARG
            ;;
        s)  socketdir=$OPTARG
            ;;
    esac
done

#print current configuration
echo "MOUNTPOINT: $mountpoint"
echo "APIVERSION: $apiversion"
socket=$socketdir/${mountpoint}.sock
echo "SOCKET: $socket"


shift $((OPTIND-1))
[ "$1" = "--" ] && shift

./build/setup-env.sh

#deploy uwsgi.ini file to vassals dir
#this triggers uwsgi process to reboot the site
sed \
    -e "s:%%plugins%%:$plugindir:" \
    -e "s:%%pwd%%:$here:" \
    -e "s:%%pyenv%%:$here/resource/env:" \
    -e "s:%%socket%%:$socket:" \
    -e "s:%%mountpoint%%:$mountpoint:" \
    -e "s:%%wsgi%%:$here/aavssite/wsgi.py:" \
    build/uwsgi.template.ini > $uwsgidir/vassals/${mountpoint}.ini


#set up folders for static files to be served directly by nginx
staticroot=$(echo $nginxdir/static/tm-emulator/$mountpoint/ | sed 's://:/:g' )
staticurl=$( echo /static/$mountpoint/ | sed 's://:/:g' )
mkdir -p $staticroot

#update the settings.py file to point to the right static files
awk '/^STATIC_ROOT/ {
print "STATIC_ROOT = \"'$staticroot'\""; next; }
/^STATIC_URL/ {
print "STATIC_URL = \"'$staticurl'\""; next; }
/^BASE_API_URL/ {
print "BASE_API_URL = \"http://127.0.0.1:5000/aavs/'$apiversion'\""; next; }
{print $0}' aavssite/settings.py > tmp.txt && mv tmp.txt aavssite/settings.py

#Do the standard django migrations
build/inve.sh $here/resource/env python manage.py makemigrations monitoring
build/inve.sh $here/resource/env python manage.py migrate
build/inve.sh $here/resource/env python manage.py collectstatic --noinput
