#!/bin/sh
#run a command in a particular virtualenv
#usage ./inve ENVFOLDER COMMAND
#ENVFOLDER should be such that ENVFOLDER/bin/python exists
env=$1
shift
export VIRTUAL_ENV=$env
export PATH="$VIRTUAL_ENV/bin:$PATH"
unset PYTHON_HOME
echo "running in virtual env: $env"
echo "$(type python)"
exec "${@:-$SHELL}"
