#!/bin/bash
# Enter test virtualenv
ENVNAME="resource/testenv"
CWD=$(pwd)
source $ENVNAME/bin/activate

# Functional tests always test what is running on the testtm server
echo "Running functional tests ..............................................."
python manage.py test $CWD/test/functional --pattern=*

