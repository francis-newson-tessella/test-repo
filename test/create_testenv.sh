#!/bin/bash
# create_testenv.sh: script to create TM Emulator test virtualenv
# This virtualenv does not depend on any system level packages
# Except: pip, virtualenv and python3

# create virtual environment (remove old if it exists) and enter it 
ENVNAME="testenv"
if [ -f resource/$ENVNAME/bin/activate ]; then
    rm -rf resource/$ENVNAME
fi
mkdir -p resource
virtualenv -p $(which python3) resource/$ENVNAME --no-site-packages
echo "Created virtualenv " $ENVNAME
source resource/$ENVNAME/bin/activate

# install standard and then testing specific packages
# (always upgrade pip to avoid warnings)
pip install --upgrade pip
PACKAGES=build/requirements.pip
TEST_PACKAGES=test/test_packages.pip
echo "Installing standard build packages"
pip install -r $PACKAGES
echo "Installing test packages"
pip install -r $TEST_PACKAGES
