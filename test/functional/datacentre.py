from selenium import webdriver
import unittest

class DataCentreTestCase(unittest.TestCase):

    def setUp(self):
        # Instantiate the selenium webdriver
        self.browser = webdriver.Firefox()

        # Simplistic, but works for now: wait up to 3 secs for page load
        self.browser.implicitly_wait(3)

    def tearDown(self):
        # close the web browser
        self.browser.quit()

    def test_GIVEN_home_url_WHEN_operator_THEN_shows_rack(self):
        # Operator goes to home page
        self.browser.get( 'http://10.30.50.80:8000/testtm' )

        # And notices the 2nd heading mentions it is the datacentre view
        heading_rows = self.browser.find_elements_by_tag_name('h1')
        self.assertTrue(
            any(row.text == 'Datacentre View' for row in heading_rows)
        )

# If run from the command line, this will ensure the testrunner is invoked
if __name__ == '__main__':
    unittest.main(warnings='ignore')
