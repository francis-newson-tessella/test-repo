#!/bin/bash
# run_tests.sh: script to run TM Emulator tests

# Enter test virtualenv
ENVNAME="resource/testenv"
CWD=$(pwd)
source $ENVNAME/bin/activate
# Unit tests always test the current branch code
echo "Running unit tests ....................................................."
python manage.py test
