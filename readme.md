# TM Emulator

##Pre-requisites

If pip and virtualenv are not installed, install them with:

- `sudo apt-get update`
- `sudo apt-get --assume-yes install python3-pip`
- `sudo apt-get --assume-yes install python3-virtualenv`


## To run the TM Emulator locally
- First set up the API from aavs-backend according to its documentation.
- Then, from the top directory of the TM Emulator, run:
    - Set up the python environment for the TM Emulator by running: `./build/setup-env.sh`
    - Run the TM Emulator with `./build/runlocal.sh`

## To deploy the TM Emulator using nginx
- Follow the instructions in ./build/readme.md
